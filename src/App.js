import { useState, useEffect } from 'react';

import './App.css';
import AppNavbar from './components/AppNavbar';


// pages
import Home from './pages/Home';
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import SpecificProduct from './pages/SpecificProduct';
import Error from './pages/Error'
import Order from './pages/Order'
import Cart from './pages/Cart'

//routing components
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Switch } from 'react-router-dom'
// Bootstrap
import { Container } from 'react-bootstrap' ;

//react context
import UserContext from './UserContext'





function App() {
	//Add stateHOOK FOR USER
	const [user,setUser] = useState({
		accessToken: localStorage.getItem('accessToken'),
		email:localStorage.getItem('email'),
		isAdmin: localStorage.getItem('isAdmin') === 'true'
	});

	//function to delete the local storage data on logout
	const unsetUser = () => {
		localStorage.clear()
	}

	//used to check if the user info is properly stored upon login and the localStorage information is cleared upon logout
	useEffect(() => {
		console.log(user)
		console.log(localStorage)
	}, [user])
	


  return (
  	//provider components allows consuming components to subscribe to context changes
  		<UserContext.Provider value = { {user,setUser, unsetUser} }>
		    <Router>
		      <AppNavbar />
		      <Container >
		      <Switch>
		      	<Route exact path="/" component = {Home} />

		      	<Route exact path="/products" component = {Products} />

				<Route exact path="/order" component = {Order} />

				<Route exact path="/cart" component ={Cart} />

		      	<Route exact path="/register" component ={Register} />

		      	<Route exact path='/login' component = {Login} />

		      	<Route exact path='/logout' component = {Logout} />

				<Route exact path='/products/:productId' component={SpecificProduct} />  

		      	<Route component = {Error} />
		     </Switch>
		      </Container>
		    </Router>
    	</UserContext.Provider>
  );
}

export default App;