import {Fragment, useEffect, useState, useContext} from 'react'
import { Form, Card } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
//routing import
import { Redirect, useHistory, Link } from 'react-router-dom'
//MUI
import { purple } from '@mui/material/colors';
import Button from '@mui/material/Button';
import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles'


export default function Login(){
	//useHistory hook gives you access to the history instance that you may use to navigate
		const history = useHistory();
		//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider component in App.js
		const { user, setUser} = useContext(UserContext)
	
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [isActive, setIsActive] = useState(false);
		//CSS MUI
		const ColorButton = styled(Button)(({ theme }) => ({
			color: theme.palette.getContrastText(purple[500]),
			backgroundColor: purple[500],
			'&:hover': {
			  backgroundColor: purple[700],
			},
		  }));

		useEffect(() => {
			if(email !== '' && password !==''){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [email, password])

		function loginUser(e){
			e.preventDefault();
			//fetch syntax 
			fetch('https://guarded-castle-11291.herokuapp.com/users/loginUser', {
        method: 'POST',
        headers: {
        	'Content-Type': 'application/json',
			
        },
        body: JSON.stringify({
        		email: email,
        		password: password
        	})
   		})
		.then(res => res.json())
		.then(data => {
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken)
				setUser({ accessToken: data.accessToken })

				Swal.fire({
					title: 'yaaay!',
					icon: 'success',
					text: 'thank you for logging in!'
				})

				//get users details

				fetch('https://guarded-castle-11291.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						
						history.push('/products')
					}else{
						
						history.push('/')
					}
				})
			} else{
				Swal.fire({
					title: 'Oops!!!',
					icon: 'error',
					text: 'invalid credentials'
				})
			}
			setEmail('')
			setPassword('')
		})
}

		
	return(
		(user.accessToken !== null) ?
			<Redirect to = "/"/>
		:
	<Fragment>
		<h1 className= "text-center"> Login </h1>
		<div className= "d-flex justify-content-center">
		<Card className="text-center">
			<Form onSubmit = {(e) => loginUser(e)} className="mr-3 ml-3"> 
				<Form.Group>
					<Form.Label> Email Address: </Form.Label>
					<Form.Control 
					type = "email" 
					placeholder = "Enter Email"
					value= {email}
					onChange={e => setEmail(e.target.value)}
					required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label> Password: </Form.Label>
					<Form.Control 
					type = "password" 
					placeholder = "Enter Password"
					value= {password}
					onChange={e => setPassword(e.target.value)}
					required
					/> 
				</Form.Group>
				{isActive ?
				<ColorButton variant="contained"  type="submit" id="submitBtn">Login</ColorButton>
				:
				<ColorButton variant="contained" disabled type="submit" id="submitBtn" >Login</ColorButton>	
				}
				<p> Not registered yet? Click here to
				<Link to={'/register'}> Register</Link>
				</p> 
			</Form>
		</Card>
		</div>

		

		</Fragment>
		)
}