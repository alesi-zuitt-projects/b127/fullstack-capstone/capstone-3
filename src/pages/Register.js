import {Fragment, useEffect, useState, useContext, React} from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { Redirect, useHistory } from 'react-router-dom'



export default function Register(){
		const {user} = useContext(UserContext)
		const [firstName, setfirstName] = useState('');
		const [lastName, setlastName] = useState('');
		const [mobileNo, setmobileNo] = useState(''); 
		const [email, setEmail] = useState('');
		const [address, setAddress] = useState('')
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		const [isActive, setIsActive] = useState(false);
		const history = useHistory();


		useEffect(() => {
			if((firstName !== "" && lastName !== "" && email !== '' && password1 !=='' && password2 !=='') && (password1 === password2) && (mobileNo.length === 11)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [firstName, lastName, mobileNo, email, address, password1, password2])

		function registerUser(e){
			e.preventDefault();

			fetch('https://guarded-castle-11291.herokuapp.com/users/checkEmail', {
		        method: "POST",
		        headers: {
		        	'Content-Type': 'application/json'
		        },
		        body: JSON.stringify({
		        		email: email  		
        		})
   			})
			.then(res => res.json())
			.then(data => {
				if(data === true){
					Swal.fire({
						title: `${email}`,
						icon: "error",
						text:'Email already in use!!!'
					})
			
				}else{
					fetch('https://guarded-castle-11291.herokuapp.com/users/registration', {
				        method: "POST",
				        headers: {
			        	'Content-Type': 'application/json'
			       		},
						body: JSON.stringify({
			        		firstName: firstName,
			        		lastName: lastName,
			        		mobileNo: mobileNo,
			        		email: email,
							address: address,
			        		password: password1
	        				})
						})
						.then(res => res.json())
						.then(data => {
							Swal.fire({
							title: `${email}`,
							icon: 'success',
							text: 'Thank you for registering!!!'

							})
								history.push('/login')
						})
				
					}			
			
				})


			
			setfirstName('')
			setlastName('')
			setmobileNo(0)
			setEmail('')
			setAddress('')
			setPassword1('')
			setPassword2('')
		
		
		}


		

	
	return(
		(user.accessToken !== null) ?
			<Redirect to = "/"/>
		:
	<Fragment>
		<h1> Register </h1>	
		<Form onSubmit = {(e) => registerUser(e)}> 
			<Form.Group>
				<Form.Label> First Name: </Form.Label>
				<Form.Control 
				type = "text" 
				placeholder = "Enter First Name"
				value= {firstName}
				onChange={e => setfirstName(e.target.value)}
				required
				/>
				
			</Form.Group>
			<Form.Group>
				<Form.Label> Last Name: </Form.Label>
				<Form.Control 
				type = "text" 
				placeholder = "Enter Last Name"
				value= {lastName}
				onChange={e => setlastName(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Contact Number: </Form.Label>
				<Form.Control 
				type='number'  
				placeholder = "Enter Contact No."
				value= {mobileNo}
				onChange={e => setmobileNo(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Email Address: </Form.Label>
				<Form.Control 
				type = "email" 
				placeholder = "Enter Email"
				value= {email}
				onChange={e => setEmail(e.target.value)}
				required
				/>
				<Form.Text className = "text-muted"> We'll never share your email with anyone else </Form.Text> 
			</Form.Group>
			<Form.Group>
				<Form.Label> Home Address: </Form.Label>
				<Form.Control 
				type = "text" 
				placeholder = "Enter Home Address"
				value= {address}
				onChange={e => setAddress(e.target.value)}
				required
				/>
				<Form.Text className = "text-muted"> We'll never share your address with anyone else </Form.Text> 
			</Form.Group>
			<Form.Group>
				<Form.Label> Password: </Form.Label>
				<Form.Control 
				type = "password" 
				placeholder = "Enter Password"
				value= {password1}
				onChange={e => setPassword1(e.target.value)}
				required
				/>
				<Form.Text className = "text-muted"> We'll never share your password with anyone else </Form.Text> 
			</Form.Group>
			<Form.Group>
				<Form.Label> Verify Password: </Form.Label>
				<Form.Control 
				type = "password"

				placeholder = "Verify your Password"
				value= {password2}
				onChange={e => setPassword2(e.target.value)}
				required
				/>
				
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">Submit </Button>
			:
			<Button variant="primary" disabled type="submit" id="submitBtn">Submit </Button>
			}
		</Form>
		</Fragment>
		)
}